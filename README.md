# pr0gramm-commentdump

Dumps the comments of a User. Depends on python3 and requests.

## Usage

```
usage: pr0com2txt.py [-h] [-u USERNAME] [-o OUTPUT] me

Dumps pr0gramm.com comments to a textfile.

positional arguments:
  me                    Content of the 'me'-cookie. Required, as the API
                        requires authentication.

optional arguments:
  -h, --help            show this help message and exit
  -u USERNAME, --username USERNAME
                        Target user. Defaults to the user of the 'me'-cookie.
  -o OUTPUT, --output OUTPUT
                        Output file. - for stdout (default).
```

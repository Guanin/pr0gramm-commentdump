#! /usr/bin/env python3

# Written by Guanin

# Requirements:
# - python3 (https://www.python.org/)
# - requests (http://docs.python-requests.org/en/master/)
#
# Help is available with --help

import requests
import json
import argparse
import time
import urllib
import sys
from datetime import datetime

API_ENDPOINT = 'https://pr0gramm.com/api/profile/comments?name={}&flags=11&before={}'

COMMENT_HEADER = '''
__                        ____                                               __      
\ \           ____  _____/ __ \   _________  ____ ___  ____ ___  ___  ____  / /______
 \ \         / __ \/ ___/ / / /  / ___/ __ \/ __ `__ \/ __ `__ \/ _ \/ __ \/ __/ ___/
 / /        / /_/ / /  / /_/ /  / /__/ /_/ / / / / / / / / / / /  __/ / / / /_(__  ) 
/_/_____   / .___/_/   \____/   \___/\____/_/ /_/ /_/_/ /_/ /_/\___/_/ /_/\__/____/  
 /_____/  /_/                                                                        


Targeted User: {}'''

COMMENT_TEMPLATE = '''
##############################
Datum: {}
Link:  https://pr0gramm.com/new/{}:comment{}
{}'''

def ts2str(ts):
    return datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

# Query the API until it worked
def query(cookies, user, timestamp=None):
    # Add missing timestamp
    if not timestamp:
        timestamp = datetime.now().timestamp()
    # Keep trying
    while True:
        try:
            r = requests.get(API_ENDPOINT.format(user, timestamp), cookies=cookies)
            return r.json()
        except Exception as e:
            print(e)
            # Sleep to prevent punishment
            time.sleep(5)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Dumps pr0gramm.com comments to a textfile.')
    parser.add_argument("-u", "--username", default=None, help="Target user. Defaults to the user of the 'me'-cookie.")
    parser.add_argument("-o", "--output", default="-", type=argparse.FileType('w', encoding='UTF-8'), help="Output file. - for stdout (default).")
    parser.add_argument("me", type=str, help="Content of the 'me'-cookie. Required, as the API requires authentication.")
    
    args = parser.parse_args()
    cookies = {"me": args.me}

    # No username given, parse me cookie
    if args.username is None:
        if args.me[-1] == ";":
            args.me = args.me[:-1];
        me = json.loads(urllib.parse.unquote(args.me))
        args.username = me["n"]
        
    print("Dumping user {}".format(args.username), file=sys.stderr)
    
    # ASCII Art generated with
    # http://patorjk.com/software/taag/#p=moreopts&h=2&v=1&f=Slant&t=
    print(COMMENT_HEADER.format(args.username), file=args.output)
    
    moreDataAvailable = True
    nextTimestamp = None

    # Keep the data coming
    while moreDataAvailable:
        print("Next Timestamp: {}".format(str(nextTimestamp)), file=sys.stderr)

        response = query(cookies, args.username, nextTimestamp)

        # Check for an error
        if u"error" in response:
            if response["code"] == 403:
                print(response["msg"])
                sys.exit(1)
            # I do not know about other errors so far
            print(response)
            break

        # Check for comments
        if len(response["comments"]) == 0:
            print("No comments found")
            break

        # Store all comments
        for c in response["comments"]:
            print(COMMENT_TEMPLATE.format(ts2str(c.get("created")),
                                          c.get("itemId", 0),
                                          c.get("id", 0),
                                          c.get("content", "")),
                  file=args.output)

        nextTimestamp = response["comments"][-1]["created"]
        moreDataAvailable = response.get("hasOlder", False)
